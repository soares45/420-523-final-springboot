package ca.claurendeau.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.Livre;

public interface LivreRepository extends JpaRepository<Livre, Long>{
    
    // VOTRE CODE ICI
}
